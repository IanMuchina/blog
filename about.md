---
title: About
permalink: /about/
layout: page
excerpt: 
comments: false
---

Hi, I'm Ian. I write here about useful things I discover. My interests are currently in Linux administration & computer security.

#### Links 
- Twitter - [@TheHackSheep](https://twitter.com/TheHackSheep)
- Github - [IanMuchina](https://github.com/IanMuchina)
- Email - [Link](https://aemail.com/OYLG) 

Thanks for stopping by
